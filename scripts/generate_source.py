#!/usr/bin/python3

import csv

NUM_FRAMES = 1430

with open('../runtime_data/profile.csv', 'r') as f:
    reader = csv.reader(f)
    for item in list(reader):
        knob = "%sx%sx%s" % (item[1], item[2], item[3]);
        skip = int(item[2])
        bw_file = "../test_data/profiling/bw-%s.csv" % knob
        with open(bw_file, 'r') as bw_f:
            bw_list = list(csv.reader(bw_f))
            for i in range(1, NUM_FRAMES + 1):
                bw = 0
                if i % (skip + 1) == 0:
                    bw = bw_list[i // (skip + 1) - 1][1]
                print("%s,%s,%s,%s,%s" % (item[1], item[2], item[3], i, bw))
