#!/usr/bin/python3

import csv

with open('../runtime/server_log_awstream.txt', 'r') as faws, open('../runtime/server_log_tcp.txt', 'r') as ftcp:
    aws = []
    for line in faws:
        if line.find('throughput') != -1:
            items = line.split()
            aws.append('%s,%s,%s' % (items[11], items[8], items[14]))

    tcp = []
    for line in ftcp:
        if line.find('throughput') != -1:
            items = line.split()
            tcp.append('%s,%s,%s' % (items[11], items[8], items[14]))

    while len(tcp) < len(aws):
        tcp.append('NA,NA,NA')

    if len(aws) < len(tcp):
        tcp = tcp[0:len(aws)]

    print('time,aws.latency,aws.throughput,aws.accuracy,tcp.latency,tcp.throughput,tcp.accuracy')
    for i in range(0, len(tcp)):
        print('%d,%s,%s' % (5 + i, aws[i], tcp[i]))
