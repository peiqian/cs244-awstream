#!/usr/bin/python3

import csv

with open('../train_data/pareto/pareto.csv', 'r') as f:
    reader = csv.reader(f)
    next(reader) # Skip header row.
    items = sorted(list(reader), key=lambda x: float(x[4]))
    for item in items:
        if float(item[4]) < 0.73 or float(item[0]) > 150000: continue
        print(",".join(item))
